function calc() {
    let kol = document.getElementById("kol").value;
    let ed = document.getElementById("ed").value;
    console.log(kol, ed);
    let r = document.getElementById("itog");
    if((kol.match(/^[0-9]*[.]?[0-9]+$/) === null) || (ed.match(/^[0-9]*[.]?[0-9]+$/) === null)){
        r.innerHTML = "Некорректно";
        alert("Некорректные данные! Введите числа > 0,вещественные числа записывайте через точку");
    }
    else {
        r.innerHTML = (kol*ed).toFixed(2);
        return false;
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button");
    b.addEventListener("click", calc);
});
